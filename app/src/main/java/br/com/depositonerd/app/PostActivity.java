package br.com.depositonerd.app;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import br.com.depositonerd.app.Models.Post;
import br.com.depositonerd.app.Services.RetrofitClient;
import br.com.depositonerd.app.Services.RetrofitService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static br.com.depositonerd.app.MainActivity.BASEURL;

public class PostActivity extends AppCompatActivity {

    private static final String TAG = "PostActivity";

    private String idPost = "";
    private ProgressBar progressBar;
    private TextView textTitle;
    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        if (getIntent().getStringExtra("ID_POST") != null)
            idPost = getIntent().getStringExtra("ID_POST");

        initToolbar();
        initRes();
        setupWebView();
        //buscaPost();
    }

    private void initToolbar(){
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

    private void initRes(){
        //textTitle = findViewById(R.id.textTitle);
        //progressBar = findViewById(R.id.progressbar);
    }

    private void setupWebView(){
        webView = findViewById(R.id.webview);
        String html = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<body>\n" +
                idPost +
                "</body>\n" +
                "</html>";

        //webView.loadData(html, "text/html", "UTF-8");

        webView.loadUrl(idPost);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient());
    }

    private void buscaPost(){

        progressBar.setVisibility(VISIBLE);

        RetrofitClient
                .getClient(BASEURL)
                .create(RetrofitService.class)
                .getPost(idPost)
                .enqueue(new Callback<Post>() {
                    @Override
                    public void onResponse(Call<Post> call, Response<Post> response) {
                        Log.d(TAG, "onResponse: ");
                        progressBar.setVisibility(GONE);
                        Post post = response.body();

                        Post.Title title = post.getTitle();
                        //textTitle.setText(title.getRendered());

                        Post.Content content = post.getContent();
                        webView.loadData(content.getRendered(), "text/html", "UTF-8");
                    }

                    @Override
                    public void onFailure(Call call, Throwable t) {
                        Log.e("TAG", "onFailure: " + t.toString());
                        progressBar.setVisibility(GONE);
                    }
                });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    @Override
    public void onBackPressed() {
        finish();
    }

}
