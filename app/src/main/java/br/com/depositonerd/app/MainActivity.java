package br.com.depositonerd.app;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;


import android.util.Log;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;

import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.depositonerd.app.Adapters.RecyclerViewAdapter;
import br.com.depositonerd.app.Models.Post;
import br.com.depositonerd.app.Services.RetrofitClient;
import br.com.depositonerd.app.Services.RetrofitService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "MainActivity";
    public static final String BASEURL = "http://depositonerd.com.br";

    private LinearLayoutManager layoutManager;
    private RecyclerView recyclerView;
    private RecyclerViewAdapter adapter;
    private List<Post> list = new ArrayList<>();
    private ProgressBar progressBar;
    private DrawerLayout drawer;

    private boolean isLoading = false;
    private Integer pagina = 1;
    private Integer categoria = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initToolbar();
        initRecyclerView();
        buscaPosts(pagina, categoria);

    }

    private void initToolbar(){
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.Open, R.string.Close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // Exibir a versao do app no sidebar
        View headerView = navigationView.getHeaderView(0);
        TextView nav_user = headerView.findViewById(R.id.txtVersao);
        nav_user.setText("Versão "+ BuildConfig.VERSION_NAME);
    }

    private void initRecyclerView(){
        recyclerView = findViewById(R.id.reciclerview);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerViewAdapter(list, this);
        recyclerView.setAdapter(adapter);

        progressBar = findViewById(R.id.progressbar);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == list.size() - 1) {
                        buscaMaisPosts(pagina, categoria);
                        pagina++;
                    }
                }
            }
        });
    }

    private void buscaPosts(final Integer page, Integer cat){

        progressBar.setVisibility(VISIBLE);
        isLoading = true;
        Log.d(TAG, "buscaPosts: "+ cat);

        RetrofitClient
                .getClient(BASEURL)
                .create(RetrofitService.class)
                .getPosts(page, cat)
                .enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                Log.d(TAG, "onResponse: ");
                progressBar.setVisibility(GONE);
                isLoading = false;

                list.addAll(response.body());
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                Log.e("TAG", "onFailure: " + t.toString());
                isLoading = false;
                progressBar.setVisibility(GONE);
            }
        });
    }


    private void buscaMaisPosts(final Integer page, Integer cat){

        isLoading = true;
        list.add(null);
        adapter.notifyItemInserted(list.size() -1);

        RetrofitClient
                .getClient(BASEURL)
                .create(RetrofitService.class)
                .getPosts(page, cat)
                .enqueue(new Callback<List<Post>>() {
                    @Override
                    public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                        Log.d(TAG, "onResponseMore: ");

                        list.remove(list.size() - 1);
                        int scrollPosition = list.size();
                        adapter.notifyItemRemoved(scrollPosition);

                        assert response.body() != null;
                        for (Post post: response.body()) {
                            list.add(post);
                        }
                        adapter.notifyDataSetChanged();
                        isLoading = false;
                    }

                    @Override
                    public void onFailure(Call call, Throwable t) {
                        Log.e("TAG", "onFailure: " + t.toString());
                        isLoading = false;
                    }
                });
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
//        return true;
//    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {

        switch (menuItem.getItemId()){
            case R.id.nav_news:
                categoria = null;
                pagina = 1;
                list.clear();
                buscaPosts(pagina,categoria);
                break;

            case R.id.nav_cinema:
                categoria = 8;
                pagina = 1;
                list.clear();
                buscaPosts(pagina,categoria);
                break;

            case R.id.nav_series:
                categoria = 11;
                pagina = 1;
                list.clear();
                buscaPosts(pagina,categoria);
                break;

            case R.id.nav_games:
                categoria = 12;
                pagina = 1;
                list.clear();
                buscaPosts(pagina,categoria);
                break;

            case R.id.nav_avalie:
                final String appPackageName = getPackageName();
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


}
