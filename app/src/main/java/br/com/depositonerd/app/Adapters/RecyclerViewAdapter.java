package br.com.depositonerd.app.Adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import br.com.depositonerd.app.Models.Post;
import br.com.depositonerd.app.PostActivity;
import br.com.depositonerd.app.R;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Post> itemList;
    private Activity activity;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    public RecyclerViewAdapter(List<Post> itemList, Activity activity) {
        this.itemList = itemList;
        this.activity = activity;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview, parent, false);
            return new RecyclerViewItemHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_load, parent, false);
            return new RecyclerViewItemHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof RecyclerViewItemHolder) {
            populateItemRows((RecyclerViewItemHolder) holder, position);
        } else if (holder instanceof LoadingViewHolder) {
            showLoadingView((LoadingViewHolder) holder, position);
        }
    }

    @Override
    public int getItemCount() {
        return itemList == null ? 0 : itemList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return itemList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    private void showLoadingView(LoadingViewHolder viewHolder, int position) {
        //ProgressBar would be displayed

    }

    private void populateItemRows(RecyclerViewItemHolder holder, int position) {

        if(itemList!=null) {
            // Get car item dto in list.
            final Post item = itemList.get(position);
            holder.item = itemList.get(position);

            if(item != null) {
                Post.Title titulo = item.getTitle();
                holder.getTitleText().setText(titulo.getRendered());
                Post.Category categoria = item.getCategory().get(0);
                holder.getCatText().setText(categoria.getName());

                if (item.getCategory().size() > 1){
                    Post.Category categoria2 = item.getCategory().get(1);
                    holder.getCat2Text().setText(categoria2.getName());
                    holder.getCat2Text().setVisibility(View.VISIBLE);
                } else {
                    holder.getCat2Text().setVisibility(View.GONE);
                }

                SimpleDateFormat spf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                Date newDate = null;
                try {
                    newDate = spf.parse(item.getDate());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Locale localeBR = new Locale("pt", "BR");
                spf= new SimpleDateFormat("d MMM yyyy - HH:mm", localeBR);

                holder.getDataText().setText(spf.format(newDate));

                String imgUrl = item.getFimgUrl();
                if (!imgUrl.isEmpty()) {
                    Picasso.with(activity)
                            .load(imgUrl)
                            .into(holder.getImagemView());
                } else {
                    holder.getImagemView().setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.sem_imagem));
                }
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Post.Guid link = item.getGuid();
                    activity.startActivity(new Intent(activity, PostActivity.class).putExtra("ID_POST", link.getRendered()));

//                    Post.Content content = item.getContent();
//                    activity.startActivity(new Intent(activity, PostActivity.class).putExtra("ID_POST", content.getRendered()));

                }
            });
        }

    }

    private class RecyclerViewItemHolder extends RecyclerView.ViewHolder {

        private View view;
        private Post item;
        private TextView titleText = null;
        private ImageView imageView = null;
        private TextView catText = null;
        private TextView cat2Text = null;
        private TextView dataText = null;

        RecyclerViewItemHolder(View itemView) {
            super(itemView);
            view = itemView;

            titleText = itemView.findViewById(R.id.card_view_image_title);
            imageView = itemView.findViewById(R.id.card_view_image);
            catText = itemView.findViewById(R.id.card_view_category);
            cat2Text = itemView.findViewById(R.id.card_view_category2);
            dataText = itemView.findViewById(R.id.card_view_data);
        }

        TextView getTitleText() {
            return titleText;
        }
        ImageView getImagemView() {
            return imageView;
        }
        TextView getCatText() {
            return catText;
        }
        TextView getCat2Text() {
            return cat2Text;
        }
        TextView getDataText() {
            return dataText;
        }
    }
}


