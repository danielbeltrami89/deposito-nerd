package br.com.depositonerd.app.Services;

import java.util.List;

import br.com.depositonerd.app.Models.Post;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RetrofitService {

    @GET("/wp-json/wp/v2/posts/")
    Call<List<Post>> getPosts(
            @Query("page") Integer pagina,
            @Query("categories") Integer categoria
    );

    @GET("/wp-json/wp/v2/posts/{id}")
    Call<Post> getPost(
            @Path("id") String id
    );
}
